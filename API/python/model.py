import sys
import pickle
import pandas as pd

from functions import getElevationData, build_df, get_location_after

positions = pd.read_csv("python/positions_df.csv") #check if path correct
positions.drop(columns="Unnamed: 0", axis=0, inplace= True)
positions['gpstime'] = pd.to_datetime(positions['gpstime'])

bases = [
    #{"name": "Rega-Center", "lat": 47.457990, "lon": 8.572991},
    {"name": "Rega_1", "lat": 47.395876, "lon": 8.637898},
    {"name": "Rega_2", "lat": 47.605896, "lon": 7.523330},
    {"name": "Rega_3", "lat": 46.909619, "lon": 7.504851},
    {"name": "Rega_4", "lat": 46.547337, "lon": 6.618223},
    {"name": "Rega_5", "lat": 46.912776, "lon": 9.551239},
    {"name": "Rega_6", "lat": 46.163171, "lon": 8.881034},
    {"name": "Rega_7", "lat": 47.405521, "lon": 9.290079},
    {"name": "Rega_8", "lat": 46.834069, "lon": 8.638286},
    {"name": "Rega_9", "lat": 46.530427, "lon": 9.878443},
    {"name": "Rega_10", "lat": 46.670054, "lon": 7.876414},
    {"name": "Rega_12", "lat": 47.078150, "lon": 9.066240},
    {"name": "Rega_14", "lat": 46.554903, "lon": 7.379185},
    {"name": "Rega_15", "lat": 46.233339, "lon": 6.096148},
    #{"name": "Trainingsbasis Grenchen", "lat": 47.181693, "lon": 7.411192}
]
bases_df = pd.DataFrame(bases)

mission_lat = float(sys.argv[1])
mission_lon = float(sys.argv[2])
alarm = sys.argv[3] # "2019-07-05 11:01:00" mission['Alarmeingang'] # to define -> maybe we can put a random time in the range of the time in missions
# needs to be in the datetime format
alarm = pd.to_datetime(alarm)

model_pickle = "python/best_model.pickle"
with open(model_pickle, "rb") as file:
  model = pickle.load(file)

for base in bases:
    unit = base["name"]
    start_location = get_location_after(alarm, unit, positions.copy(), bases_df.copy())
    base_lat = start_location[1][0]
    base_lon = start_location[1][1]

    preds = pd.DataFrame()

    elevation_data = getElevationData(base_lat,
                                      base_lon,
                                      mission_lat,
                                      mission_lon)

    input = [
        {
        "WGS84_Latitude":  mission_lat,
        "WGS84_Longitude":  mission_lon,
        "liftoff_lat":  base_lat,
        "liftoff_lng":  base_lon,
        "climb":  elevation_data['climb'],
        "descent":  elevation_data['descent'],
        "vert_travel_dist":  elevation_data['vert_travel_dist']
        }
    ]
    input_df = pd.DataFrame(input)
    X_test = build_df(input_df)
    pred = model.predict(X_test)[0]
    pred = round(pred/60)

    # where do you take the min
    print(unit,pred,base_lat,base_lon)
