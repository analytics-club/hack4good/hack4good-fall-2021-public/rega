import requests
import json
import pandas as pd
from datetime import datetime, timedelta
from geopy.distance import distance

def getElevationData(lat_start, lon_start, lat_end, lon_end):
    """Get elevation data of direct path between two locations

    Arguments:
    lat_start -- Latitude of starting position
    lon_start -- Longitude of starting position
    lat_end -- Latitude of ending position
    lon_end -- Longitude of ending position

    Returns:
    Dictionary with
        climb -- total climb distance (meters)
        descent -- total descent distance (meters)
        vert_travel_dist -- total vertical travel distance (meters)
    """
    api_baseURL = "https://api.elevationapi.com/api/Elevation/line"
    requestURL = f"{api_baseURL}/{lat_start},{lon_start}/{lat_end},{lon_end}"

    request = requests.get(requestURL)
    request.raise_for_status() # Throws exception if API call was not successful

    response_json = request.json()

    result = dict()
    result["climb"] = response_json["metrics"]["climb"]
    result["descent"] = abs(response_json["metrics"]["descent"])
    result["vert_travel_dist"] = result["climb"] + result["descent"]

    return result


def build_df(data):
  lat_delta = (data['liftoff_lat'] - data['WGS84_Latitude']).abs()
  lng_delta = (data['liftoff_lng'] - data['WGS84_Longitude']).abs()
  X_new = pd.concat((lat_delta, lng_delta, data["climb"], data["descent"],
                     data["vert_travel_dist"]), axis=1)
  X_new.rename({0: "lat_delta", 1: "lng_delta"}, inplace=True, axis=1)

  X_new['distance'] = data.apply(
      lambda r: distance(
          (r['liftoff_lat'], r['liftoff_lng']),
          (r['WGS84_Latitude'], r['WGS84_Longitude'])).km, axis = 1 )
  return X_new

def get_location_after(start_time: datetime, callsign: str, positions, bases_df):
    x = positions[positions['unid'] == callsign]

    # Select only positions after start time
    x = x[x['gpstime'] >= start_time]
    # Select positions not later than 30 minutes
    x = x[x['gpstime'] <= (start_time + timedelta(minutes=30))]
    if x.shape[0] <= 0:
        return start_time, (bases_df[bases_df["name"]==callsign].lat.values[0], bases_df[bases_df["name"]==callsign].lon.values[0])
    else:
        # Select the oldest position
        idx = x['gpstime'].idxmin() # Return index of first occurrence of minimum over the rows
        return x.loc[idx, 'gpstime'], (x.loc[idx, 'latitude'], x.loc[idx, 'longitude']), x.loc[idx, "unitstatus"]
