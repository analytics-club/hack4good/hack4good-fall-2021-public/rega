const {spawn} = require('child_process');
const app = require('express')();
const cors = require("cors")
const PORT = 8080;

app.use(cors())

app.listen(
  PORT,
  () => console.log(`Server started on port ${PORT}`)
)


app.get("/random", (req,res) => {
  res.send({
    "prediction": Math.random() * 15
  })
})

app.get("/predict", (req,res) => {
  var dataToSend;

  const mission_lat = req.query.mission_lat;
  const mission_lon = req.query.mission_lon;
  const time = req.query.time;
  console.log(time)
  // spawn new child process to call the python script
  const python = spawn('python', ['python/model.py', mission_lat, mission_lon,
                                  time]);


  // collect data from script
  python.stdout.on('data', function (data) {
    console.log('Running Python model ...');
    dataToSend = data.toString();
  });
  console.log(mission_lat)
  console.log(mission_lon)
  console.log(time)
  // in close event we are sure that stream from child process is closed
  python.on('close', (code) => {
    console.log(dataToSend)
    console.log(`Python process closed code ${code}`);
    // send data to browser
    var prediction_lines = dataToSend.split("\n")
    var output = []
    for (var i = 0; i < prediction_lines.length; i++) {
      prediction_line = prediction_lines[i]
      base = prediction_line.split(" ")[0]
      if(base === "")
      {
        continue
      }
      prediction = prediction_line.split(" ")[1]
      base_lat = prediction_line.split(" ")[2]
      base_lon = prediction_line.split(" ")[3]
      output.push({"base": base,"prediction": prediction, "base_lat": base_lat, "base_lon": base_lon})
    }
    console.log("output")
    console.log(output)
    res.send({
      output
    })
  });

})
