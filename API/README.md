# API README
This simple RESTful API allows prediction requests to be made over HTTP(S).

### Running the API server
The API server runs on Node.js and can be ran on any OS.  
Find out how to install Node.js on your system on the website [nodejs.org]("https://nodejs.org/en/").  
Once you have installed Node.js, you can run the server by navigating to the API folder and running `node .` on your command line. The first time you run the server, you might need to run the `npm install` in the API folder first.
By default the server runs on port 8080, but this can easily be changed in inside `index.js`

### Server documentation
The Node.js API consists of very little code.
There is only one relevant endpoint: `GET/predict`.
This endpoint takes 4 arguments:

- `mission_lat`: Mission latitude
- `mission_lon`: Mission longitude
- `time`: Datetime of mission

The model calls the Python script `model.py` passing the above arguments and reading from STDOUT to generate a HTTP response. 

It returns a JSON array of the estimated ETA for all helicopters (called 'bases' in the API), along with their last known location at the datetime of the mission.  
Example output:
```json
		[
		  {
		    base: 'Rega_1',
		    prediction: '17.0',
		    base_lat: '47.395876',
		    base_lon: '8.637898'
		  },
		  {
		    base: 'Rega_2',
		    prediction: '19.0',
		    base_lat: '47.605896',
		    base_lon: '7.52333'
		  },
		  {
		    base: 'Rega_3',
		    prediction: '13.0',
		    base_lat: '46.909619',
		    base_lon: '7.504851'
		  },
		  {
		    base: 'Rega_4',
		    prediction: '40.0',
		    base_lat: '47.227945',
		    base_lon: '5.961285'
		  },
		  {
		    base: 'Rega_5',
		    prediction: '35.0',
		    base_lat: '46.912776',
		    base_lon: '9.551239'
		  },
		  {
		    base: 'Rega_6',
		    prediction: '42.0',
		    base_lat: '46.0176083333333',
		    base_lon: '8.9533416666667'
		  },
		  {
		    base: 'Rega_7',
		    prediction: '28.0',
		    base_lat: '47.405521',
		    base_lon: '9.290079'
		  },
		  {
		    base: 'Rega_8',
		    prediction: '19.0',
		    base_lat: '46.834069',
		    base_lon: '8.638286'
		  },
		  {
		    base: 'Rega_9',
		    prediction: '46.0',
		    base_lat: '46.530427',
		    base_lon: '9.878443'
		  },
		  {
		    base: 'Rega_10',
		    prediction: '15.0',
		    base_lat: '46.670054',
		    base_lon: '7.876414'
		  },
		  {
		    base: 'Rega_12',
		    prediction: '24.0',
		    base_lat: '47.07815',
		    base_lon: '9.06624'
		  },    
		  {   
		    base: 'Rega_14',
		    prediction: '22.0',
		    base_lat: '46.554903',
		    base_lon: '7.379185'
		  },
		  {  
		    base: 'Rega_15',
		    prediction: '44.0',
		    base_lat: '46.233339',
		    base_lon: '6.096148'
		  }
		]
```