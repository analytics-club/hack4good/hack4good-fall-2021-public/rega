// import * as moment from '../node_modules/moment/src/moment.js';


export function getDistanceKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }
  
function deg2rad(deg) {
    return deg * (Math.PI/180)
}

export function mode(arr){
  return arr.sort((a,b) =>
        arr.filter(v => v===a).length
      - arr.filter(v => v===b).length
  ).pop();
}

export var bases = {
  "Rega-Center": {name: "Rega-Center", lat: 47.457990, lon: 8.572991},
  "Rega_1": {name: "Rega 1", lat: 47.395876, lon: 8.637898},
  "Rega_2": {name: "Rega 2", lat: 47.605896, lon: 7.523330},
  "Rega_3": {name: "Rega 3", lat: 46.909619, lon: 7.504851},
  "Rega_4": {name: "Rega 4", lat: 46.547337, lon: 6.618223},
  "Rega_5": {name: "Rega 5", lat: 46.912776, lon: 9.551239},
  "Rega_6": {name: "Rega 6", lat: 46.163171, lon: 8.881034},
  "Rega_7": {name: "Rega 7", lat: 47.405521, lon: 9.290079},
  "Rega_8": {name: "Rega 8", lat: 46.834069, lon: 8.638286},
  "Rega_9": {name: "Rega 9", lat: 46.530427, lon: 9.878443},
  "Rega_10": {name: "Rega 10", lat: 46.670054, lon: 7.876414},
  "Rega_12": {name: "Rega 12", lat: 47.078150, lon: 9.066240},
  "Rega_14": {name: "Rega 14", lat: 46.554903, lon: 7.379185},
  "Rega_15": {name: "Rega 15", lat: 46.233339, lon: 6.096148},
  "Rega foo": {name: "Trainingsbasis Grenchen", lat: 47.181693, lon: 7.411192}
};

export function distanceToBase(base, lat, lng) {
  var lat_base = bases[base]['lat']
  var lng_base = bases[base]['lon']
  return getDistanceFromLatLonInKm(lat, lng, lat_base, lng_base);
}

export function parseTimeToMinutes(timedelta) {
  // This function parses a python timedelta to the number of minutes,
  // discarding the number of days.
  var fromIdx = 7;  // format: 0 days 00:00:00
  var timeStr = timedelta.slice(fromIdx, -1);
  var hours = parseInt(timeStr.slice(0, 2));
  var minutes = parseInt(timeStr.slice(3, 5));
  // var seconds = timeStr.slice(6, 8); // doesn't work for some reason...
  var seconds = 0;
  // console.log("h", hours, "m", minutes, "s", seconds);
  var totalMinutes = seconds / 60 + minutes + hours * 60;
  return totalMinutes;
}
