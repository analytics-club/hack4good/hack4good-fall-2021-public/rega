function draw_map(data, filterFunction) {
    // remove all markers
    for(var i = 0; i<mission_markers.length; i++) {
        mission_markers[i].removeFrom(map);
    }
    mission_markers = [];


    if(filterFunction != null) {
        data = data.filter(filterFunction);
    }

    var t1_max = -Infinity;
    var t1_avg = 0;
    for(var i = 1; i<data.length; i++) {
        if(Number(data[i][27]) > t1_max) {
            t1_max = Number(data[i][27]);
        }
        if(!isNaN(Number(data[i][27]))) {
            t1_avg += Number(data[i][27]);
        }
    }
    t1_avg /= data.length - 1;

    for(var i = 1; i<data.length; i++) {
        lat = data[i][19];
        lon = data[i][20];

        einsatznummer = data[i][1];
        einsatzstichwort = data[i][4];

        if(typeof data[i][10] !== 'undefined') {
            var r = Math.floor(Math.min(1, data[i][27] / t1_avg) * 255);
            var g = Math.floor((1 - data[i][27] / t1_max) * 255);

            function componentToHex(c) {
                var hex = c.toString(16);
                return hex.length == 1 ? "0" + hex : hex;
            }
            function rgbToHex(r, g, b) {
                return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
            }

            var marker = L.circle([lat, lon], {
                color: rgbToHex(r, g, 0),
                fillColor: 'red',
                radius: 10
            }).bindPopup(
                "Einsatznummer:   " + data[i][1] + "<br>" +
                "Einsatzstichwort:   " + data[i][4] + "<br>" +
                "Erstellungszeit:   " + data[i][5] + "<br>" +
                "Alarmeingang:   " + data[i][6] + "<br>" +
                "Abschlusszeit:   " + data[i][7] + "<br>" +
                "Einsatzmittel:   " + data[i][10] + "<br>" +
                "Helikopter alarmiert:   " + data[i][12] + "<br>" +
                "Ankunft Einsatzort:   " + data[i][13] + "<br>" +
                "Abflug Einsatzort:   " + data[i][14] + "<br>" +
                "Ankunft Zielort:   " + data[i][15] + "<br>" +
                "Einsatzort:   " + data[i][16] + "<br>" +
                "Zielort:   " + data[i][21] + "<br>"
            );
            mission_markers.push(marker);
            marker.addTo(map);
        }
    }
}


// global state for current selection
var heli_checked = {};
for(var i = 0; i<einsatzmittel.length; i++) {
    heli_checked[einsatzmittel[i]] = true;
}
var stichwort_checked = {};
for(var i = 0; i<stichworte.length; i++) {
    stichwort_checked[stichworte[i]] = true;
}
var date_from = new Date("1970");
var date_until = new Date("2100");


// update functions
function helicopterSelected(object) {
    var heli = object.id.toString();
    heli_checked[heli] = object.checked;

    map_plot_refresh();
}
function stichwortSelected(object) {
    var sw = object.id.toString();
    stichwort_checked[sw] = object.checked;

    map_plot_refresh();
}
function dateSelected() {
    date_from = new Date(document.getElementById('date-from').value);
    date_until = new Date(document.getElementById('date-until').value);

    map_plot_refresh();
}


function map_plot_refresh() {

   var filter = function(data) {
       var mission_begin = new Date(data[6].toString())
       var mission_end = new Date(data[7].toString())

       return  heli_checked[data[10].toString()] &&
               stichwort_checked[data[3].toString()] &&
               date_from < mission_end && date_until > mission_begin;

   };

   draw_map(global_data, filter);

   draw_time_plot(global_data, 27, "time_t1", filter);
   draw_time_plot(global_data, 28, "time_t2", filter);
   draw_time_plot(global_data, 29, "time_patient", filter);
   draw_time_distribution(global_data, "time_distribution", filter);
   draw_time_line(global_data, "time_line", filter);
}


var global_data;

function draw_time_plot(data, time, time_string, filterFunction) {

    d3.selectAll('#'+time_string).remove();

    if(filterFunction != null) {
        data = data.filter(filterFunction);
    }
    var values = data.map(entry => parseInt(entry[time]));

    var formatCount = d3.format(",.0f");

    var margin = {top: 20, right: 50, bottom: 30, left: 30},
        width = 500 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

    var max = d3.max(values);
    var min = d3.min(values);
    var avg = values.reduce((a, b) => (a + b)) / values.length;

    var x = d3.scale.linear()
              .domain([min, max])
              .range([0, width]);

    var data = d3.layout.histogram()
                 .bins(x.ticks(70))
    (values);

    var yMax = d3.max(data, function(d){return d.length});
    var yMin = d3.min(data, function(d){return d.length});

    var y = d3.scale.linear()
              .domain([0, yMax])
              .range([height, 0]);

    var xAxis = d3.svg.axis()
                  .scale(x)
                  .orient("bottom");

    var svg = d3.select("#plot-"+time_string).append("svg")
                .attr("id", time_string)
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .style("margin-left", "20px")
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    svg.append("text")
       .attr("x", (width / 2))
       .attr("y", 0)
       .attr("text-anchor", "middle")
       .style("font-size", "16px")
       .style("text-decoration", "underline")
       .text(time_string);

    bar = svg.selectAll(".bar")
             .data(data)
             .enter().append("g")
             .attr("class", "bar")
             .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });


    if(time_string == "time_t1") {
       bar.append("rect")
       .attr("x", 1)
       .attr("width", (x(data[0].dx) - x(0)) - 1)
       .attr("height", function(d) { return height - y(d.y); })
       .attr("fill", function(d) {
               r = Math.min(1, d.x/avg)*255;
               g = (1-d.x/max)*255;
               return d3.rgb(r,g,0);
           });
    }else {
       bar.append("rect")
       .attr("x", 1)
       .attr("width", (x(data[0].dx) - x(0)) - 1)
       .attr("height", function(d) { return height - y(d.y); })
       .attr("fill", function(d) {
               return d3.rgb(52, 152, 219)
       });
    }


    svg.append("g")
       .attr("class", "x axis")
       .attr("transform", "translate(0," + height + ")")
       .call(xAxis);


}

function draw_time_distribution(data, desc, filterFunction) {

   d3.selectAll('#'+desc).remove();

   if(filterFunction != null) {
       data = data.filter(filterFunction);
   }

   var values = data.map(entry => new Date(entry[6].toString()).getHours());

   var formatCount = d3.format(",.0f");

   var margin = {top: 20, right: 50, bottom: 30, left: 40},
       width = 500 - margin.left - margin.right,
       height = 300 - margin.top - margin.bottom;


   var x = d3.scale.linear()
           .domain([0, 24])
           .range([0, width]);

   var data = d3.layout.histogram()
               .bins(x.ticks(24))(values);

   var yMax = d3.max(data, function(d){return d.length});
   var yMin = d3.min(data, function(d){return d.length});

   var y = d3.scale.linear()
           .domain([0, yMax])
           .range([height, 0]);

   var xAxis = d3.svg.axis()
               .scale(x)
               .orient("bottom");

   var svg = d3.select("#plot-"+desc).append("svg")
               .attr("id", desc)
               .attr("width", width + margin.left + margin.right)
               .attr("height", height + margin.top + margin.bottom)
               .append("g")
               .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


   svg.append("text")
   .attr("x", (width / 2))
   .attr("y", -5)
   .attr("text-anchor", "middle")
   .style("font-size", "14px")
   .style("text-decoration", "underline")
   .text("Verteilung 24h");

   bar = svg.selectAll(".bar")
           .data(data)
           .enter().append("g")
           .attr("class", "bar")
           .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

   bar.append("rect")
   .attr("x", 1)
   .attr("width", (x(data[0].dx) - x(0)) - 1)
   .attr("height", function(d) { return height - y(d.y); })
   .attr("fill", function(d) {
           return d3.rgb(52, 152, 219)
   });

   svg.append("g")
   .attr("class", "x axis")
   .attr("transform", "translate(0," + height + ")")
   .call(xAxis);

   var yAxis = d3.svg.axis().scale(y).orient("left");
   svg.append("g").attr("class", "y axis").call(yAxis);


}

function draw_time_line(data, desc, filterFunction) {

   d3.selectAll('#'+desc).remove();

   if(filterFunction != null) {
       data = data.filter(filterFunction);
   }

   // Count number of occurences per day and save them as tuples in result
   var dataset = {};
   for(var i = 0; i < data.length; i++) {
       var temp = new Date(data[i][6].toString());
       temp = temp.getFullYear() + "-" + (temp.getMonth() + 1) + "-" + temp.getDate();

       if(temp in dataset) {
           dataset[temp] += 1;
       }else{
           dataset[temp] = 1;
       }
   }

   var result = [];
   Object.keys(dataset).forEach(function(key) {
       var obj = {"date" : d3.time.format("%Y-%m-%d").parse(key), "count" : dataset[key]};
       result.push(obj);
   });

   console.log(result);

   // the date range of available data:
   var x_range = d3.extent(result, function(d) { return d.date; });
   // maximum date range allowed to display
   var mindate = x_range[0], maxdate = x_range[1];

   var svg = d3.select("#plot-"+desc).append("svg")
               .attr("id", desc)
               .attr("width", 1000)
               .attr("height", 300)
               .style("margin-left", "20px")
               .append("g")
               .attr("transform", "translate(" + 40 + "," + 20 + ")");

   var x = d3.time.scale()
               .range([0, 950])
               .domain([mindate, maxdate]);

   var x_axis = d3.svg.axis()
               .scale(x)
               .orient("bottom")
               .ticks(d3.time.months, 1)
               .tickFormat(d3.time.format('%m/%Y'))//%Y-for year boundaries, such as 2011
               .tickSubdivide(1);//subdivide 12 months in a year

   svg.append("g").attr("transform", "translate(0,250)").call(x_axis);


   var y = d3.scale.linear()
               .domain([0, d3.max(result, function(d) { return d.count; })])
               .range([250, 0]);

   var y_axis = d3.svg.axis().scale(y).orient("left");



   svg.append("g").attr("class", "y axis").call(y_axis);

   svg.append("path")
       .datum(result)
       .attr("fill", "none")
       .attr("stroke", "steelblue")
       .attr("stroke-width", 1.5)
       .attr("d", d3.svg.line()
               .x(function(d) { return x(d.date) })
               .y(function(d) { return y(d.count) }));

   svg.append("text")
       .attr("x", 500)
       .attr("y", -5)
       .attr("text-anchor", "middle")
       .style("font-size", "14px")
       .style("text-decoration", "underline")
       .text("Einsätze/Tag");

}