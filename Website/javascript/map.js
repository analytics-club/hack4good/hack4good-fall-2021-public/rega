var map = L.map('map').setView([46.79, 8.32], 8);

// add helicopter bases to map
var bases = [
    {name: "Rega_1", lat: 47.395876, lon: 8.637898, vehicleWeight: -1.2288906},
    {name: "Rega_2", lat: 47.605896, lon: 7.523330, vehicleWeight: -1.07300319},
    {name: "Rega_3", lat: 46.909619, lon: 7.504851, vehicleWeight: -1.51707714 },
    {name: "Rega_4", lat: 46.547337, lon: 6.618223, vehicleWeight: -1.20581611},
    {name: "Rega_5", lat: 46.912776, lon: 9.551239, vehicleWeight: 0.33669062},
    {name: "Rega_6", lat: 46.163171, lon: 8.881034, vehicleWeight: 0.68802531},
    {name: "Rega_7", lat: 47.405521, lon: 9.290079, vehicleWeight: -1.10553061},
    {name: "Rega_8", lat: 46.834069, lon: 8.638286, vehicleWeight: 1.62407988},
    {name: "Rega_9", lat: 46.530427, lon: 9.878443, vehicleWeight: 0.48542489},
    {name: "Rega_10", lat: 46.670054, lon: 7.876414, vehicleWeight: 0.35597018},
    {name: "Rega_12", lat: 47.078150, lon: 9.066240, vehicleWeight: -0.07708018},
    {name: "Rega_14", lat: 46.554903, lon: 7.379185, vehicleWeight: 0.04101756},
    {name: "Rega_15", lat: 46.233339, lon: 6.096148, vehicleWeight: 1.36202831},
    //{name: "Trainingsbasis Grenchen", lat: 47.181693, lon: 7.411192}
    //{name: "Rega-Center", lat: 47.457990, lon: 8.572991},
];
//Rega17 -> 10
//Rega17 -> 10
// 'Rega_16',  'Rega_17',
//0.01523228  0.10846541

var baseToVehicle = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var heli_icon = L.icon({
    iconUrl: 'images/heli_icon.png',
    iconSize:     [80, 50], // size of the icon
});

var heli_markers = []

function drawHelicopters()
{
  heli_markers.forEach(marker => {
    map.removeLayer(marker)
  });
  for(var i = 0; i<bases.length; i++) {
      marker = L.marker([bases[i].lat, bases[i].lon], {icon: heli_icon}).bindPopup(bases[i].name).addTo(map);
      heli_markers.push(marker)
  }
}
drawHelicopters()


// add map tiles
L.tileLayer('https://tile.osm.ch/switzerland/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18
}).addTo(map);


// create different layers for parts of UI
var predictionLayer = L.layerGroup()
var heatMapLayer = L.layerGroup()
var serviceRegionLayer = L.layerGroup()

// checkbox event handler
function togglePrediction(object) {
    if(object.checked) {
        predictionLayer.clearLayers()
        predictionLayer.addTo(map)
    } else {
        predictionLayer.removeFrom(map)
    }
}
function toggleHeat(object) {
    if(object.checked) {
        heatMapLayer.addTo(map)
    } else {
        heatMapLayer.removeFrom(map)
    }
}
function toggleService(object) {
    if(object.checked) {
        serviceRegionLayer.addTo(map)
    } else {
        serviceRegionLayer.removeFrom(map)
    }
}



// --------------------------- PREDICTION LAYER ----------------------------------------------

// Init colors
var gRainbow = null;
var gKnnTree = null;
var gKnnTrees = new Map();

function initColors() {
    import('../libs/rainbowvis.js').then(function(obj) {
        window.gRainbow = new obj.Rainbow();
        window.gRainbow.setNumberRange(10, 25);
        window.gRainbow.setSpectrum('green', 'yellow', 'red');
    });
}



initColors();


// select point on map
var selectedPoint = L.marker()
var prevLines = null


function updateMarkerPos(lat, lon) {
    // calculate all distances


    var time = $("#date_time").val()
    var api_output = fetch_predictions(lat, lon, time)
    var preds = bases.map((bases, i) => get_prediction(i, api_output))

    // Redraw helicopters with updates positions
    drawHelicopters()

    // sort indices w.r.t. preds
    var indices = new Array(preds.length)
    for(var i = 0; i < preds.length; i++) indices[i] = i
    indices = indices.sort(function (a, b) { return preds[a] < preds[b] ? -1 : preds[a] > preds[b] ? 1 : 0; })

    // draw lines to 3 nearest ones
    if(prevLines != null) prevLines.map(l => l.removeFrom(predictionLayer))
    colors = ['green', 'orange', 'red']


    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    function timeToColor(time) {
        if (window.gRainbow != null) {
            return "#" + window.gRainbow.colourAt(time);
        } else {
            t1_avg = 10   // set proper values here
            t1_max = 23.5   // set proper values here
            var r = Math.floor(Math.min(1, time / t1_avg) * 255);
            var g = Math.floor((1 - time / t1_max) * 255);
            return rgbToHex(r, g, 0)
        }
    }

    var lines = indices.slice(0, 3).map(
        (i, idx) => L.polyline(
            [[lat, lon], [bases[i].lat, bases[i].lon]],
            {color: timeToColor(preds[i])})
    )
    var labels = indices.slice(0, 3).map(
        function (i, idx) {
            var content = "<b>ETA:</b> " + preds[i].toFixed(2) + ' mins<br>';
            return L.popup()
                .setLatLng([bases[i].lat, bases[i].lon])
                .setContent(content)
        }
    )
    lines.map(l => l.addTo(predictionLayer))
    labels.map(l => l.addTo(predictionLayer))
    prevLines = lines
}

function onMapClick(e) {
    selectedPoint.setLatLng(e.latlng).addTo(predictionLayer)
    const latField = document.getElementById("latField")
    const lonField = document.getElementById("lonField")
    latField.value = e.latlng.lat
    lonField.value = e.latlng.lng
    updateMarkerPos(e.latlng.lat, e.latlng.lng)
}
map.on('click', onMapClick);

function onOkClick() {
    lat = document.getElementById("latField").value
    lon = document.getElementById("lonField").value
    selectedPoint.setLatLng([lat, lon]).addTo(predictionLayer)
    updateMarkerPos(lat, lon)
}

predictionLayer.addTo(map)


// helper functions
function fetch_predictions(p_lat, p_lon, time){
  //Make REST API call to model
  var predictions

  $.ajax({
      url: "http://localhost:8080/predict",
      async: false,
      dataType: 'json',
      type: "get",
      data: {
        mission_lat: p_lat,
        mission_lon: p_lon,
        time: time
      },
      success: function(data) {
        predictions = data["output"]
      }
  });

  return predictions;
}

function get_prediction(base, predictions) {
  //Make REST API call to model
  base_unit = bases[base]["name"]
  var eta
    predictions.forEach(prediction => {
      base_name = prediction["base"]
      if(base_unit == base_name){
        eta = parseFloat(prediction["prediction"])
        base_lat = parseFloat(prediction["base_lat"])
        base_lon = parseFloat(prediction["base_lon"])

        //UPDATE BASE POS
        bases[base]["lat"] = base_lat
        bases[base]["lon"] = base_lon

      }
    });
    return eta;

}

function distance(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        return dist * 1.609344;
    }
}
