function htmlToElem(html) {
    let temp = document.createElement('template');
    html = html.trim();
    temp.innerHTML = html;
    return temp.content.firstChild;
}

// add einsatzmittel checkboxes to DOM
let einsatzmittel = ["Rega_1", "Rega_2", "Rega_3", "Rega_4", "Rega_5", "Rega_6", "Rega_7", "Rega_8", "Rega_9", "Rega_10", "Rega_12", "Rega_14", "Rega_15", "Rega_16", "Rega_17", "AGL_05", "AGL_06", "AGL_07", "AGL_08", "Gallus_3", "Robin_3", "HBE_ZMU", "HBE_ZMI", "HBE_ZMY", "SAR02", "CH_L"];
for(var i = 0; i<einsatzmittel.length; i++) {
    let emCheckbox = htmlToElem('<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="'+einsatzmittel[i]+'">'+
                                '<input type="checkbox" id="'+einsatzmittel[i]+'" class="mdl-checkbox__input" onchange="helicopterSelected(this)" checked>'+
                                '<span class="mdl-checkbox__label">'+einsatzmittel[i]+'</span>'+
                                '</label>');
    document.getElementById('einsatzmittelSelection').appendChild(emCheckbox);
}

// add einsatzstichwort checkboxes to DOM
let stichworte = ["KRAN", "SPPI", "VEST", "UNUN", "BEWE", "PRIM", "SPBI", "UNHA", "REAN", "BEUN", "GLAB", "SPOF", "ARAR", "UNTI", "BEKL", "SPSP", "ARBA", "UNVE", "UNER", "SPSC", "ARLA", "ARFO", "SPRE", "LAGA", "GLBA", "SPTA", "LAUN", "SPCA", "FLRE", "SPBA", "GLUN", "SPLA", "UNWA", "FLZI", "BESP", "HOHL", "VEBA", "LAVE", "UNIN", "EVSE", "SPMO", "SPSW", "FLMI", "EVMA", "GLKA", "EVEV"];
for(var i = 0; i<stichworte.length; i++) {
    let swCheckbox = htmlToElem('<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="'+stichworte[i]+'">'+
                                '<input type="checkbox" id="'+stichworte[i]+'" class="mdl-checkbox__input" onchange="stichwortSelected(this)" checked>'+
                                '<span class="mdl-checkbox__label">'+stichworte[i]+'</span>'+
                                '</label>');
    document.getElementById('stichwortSelection').appendChild(swCheckbox);
}


var map = L.map('map').setView([46.79, 8.32], 8);
var heli_layers = {};

// add helicopter bases to map
var bases = [
//    {name: "Rega-Center", lat: 47.457990, lon: 8.572991},
    {name: "Rega 1", lat: 47.395876, lon: 8.637898},
    {name: "Rega 2", lat: 47.605896, lon: 7.523330},
    {name: "Rega 3", lat: 46.909619, lon: 7.504851},
    {name: "Rega 4", lat: 46.547337, lon: 6.618223},
    {name: "Rega 5", lat: 46.912776, lon: 9.551239},
    {name: "Rega 6", lat: 46.163171, lon: 8.881034},
    {name: "Rega 7", lat: 47.405521, lon: 9.290079},
    {name: "Rega 8", lat: 46.834069, lon: 8.638286},
    {name: "Rega 9", lat: 46.530427, lon: 9.878443},
    {name: "Rega 10", lat: 46.670054, lon: 7.876414},
    {name: "Rega 12", lat: 47.078150, lon: 9.066240},
    {name: "Rega 14", lat: 46.554903, lon: 7.379185},
    {name: "Rega 15", lat: 46.233339, lon: 6.096148},
//    {name: "Trainingsbasis Grenchen", lat: 47.181693, lon: 7.411192}
];

var heli_icon = L.icon({
    iconUrl: 'images/heli_icon.png',
    iconSize:     [80, 45], // size of the icon
});
for(var i = 0; i<bases.length; i++) {
    L.marker([bases[i].lat, bases[i].lon], {icon: heli_icon}).bindPopup(bases[i].name).addTo(map);
}

L.tileLayer('https://tile.osm.ch/switzerland/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18
}).addTo(map);

var mission_markers = [];

function draw_map(data, filterFunction) {
    // remove all markers
    for(var i = 0; i<mission_markers.length; i++) {
        mission_markers[i].removeFrom(map);
    }
    mission_markers = [];


    if(filterFunction != null) {
        data = data.filter(filterFunction);
    }

    var t1_max = -Infinity;
    var t1_avg = 0;
    for(var i = 1; i<data.length; i++) {
        if(Number(data[i][26]) > t1_max) {
            t1_max = Number(data[i][26]);
        }
        if(!isNaN(Number(data[i][26]))) {
            t1_avg += Number(data[i][26]);
        }
    }
    t1_avg /= data.length - 1;

    for(var i = 1; i<data.length; i++) {
        lat = data[i][18];
        lon = data[i][19];

        einsatznummer = data[i][0];
        einsatzstichwort = data[i][3];

        if(typeof data[i][9] !== 'undefined') {
            var r = Math.floor(Math.min(1, data[i][26] / t1_avg) * 255);
            var g = Math.floor((1 - data[i][26] / t1_max) * 255);

            function componentToHex(c) {
                var hex = c.toString(16);
                return hex.length == 1 ? "0" + hex : hex;
            }
            function rgbToHex(r, g, b) {
                return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
            }

            var marker = L.circle([lat, lon], {
                color: rgbToHex(r, g, 0),
                fillColor: 'red',
                radius: 10
            }).bindPopup(
                "Einsatznummer:   " + data[i][0] + "<br>" +
                    "Einsatzstichwort:   " + data[i][3] + "<br>" +
                    "Erstellungszeit:   " + data[i][4] + "<br>" +
                    "Alarmeingang:   " + data[i][5] + "<br>" +
                    "Abschlusszeit:   " + data[i][6] + "<br>" +
                    "Einsatzmittel:   " + data[i][9] + "<br>" +
                    "Helikopter alarmiert:   " + data[i][11] + "<br>" +
                    "Ankunft Einsatzort:   " + data[i][12] + "<br>" +
                    "Abflug Einsatzort:   " + data[i][13] + "<br>" +
                    "Ankunft Zielort:   " + data[i][14] + "<br>" +
                    "Einsatzort:   " + data[i][15] + "<br>" +
                    "Zielort:   " + data[i][20] + "<br>" +
                    "time_t1:   " + data[i][26] + " mins<br>" +
                    "time_t2:   " + data[i][27] + " mins<br>" +
                    "time_patient:   " + data[i][28] + " mins<br>"
            );
            mission_markers.push(marker);
            marker.addTo(map);
        }
    }
}


// global state for current selection
var heli_checked = {};
for(var i = 0; i<einsatzmittel.length; i++) {
    heli_checked[einsatzmittel[i]] = true;
}
var stichwort_checked = {};
for(var i = 0; i<stichworte.length; i++) {
    stichwort_checked[stichworte[i]] = true;
}
var date_from = new Date("1970");
var date_until = new Date("2100");


// update functions
function helicopterSelected(object) {
    var heli = object.id.toString();
    heli_checked[heli] = object.checked;

    map_plot_refresh();
}
function stichwortSelected(object) {
    var sw = object.id.toString();
    stichwort_checked[sw] = object.checked;

    map_plot_refresh();
}
function dateSelected() {
    date_from = new Date(document.getElementById('date-from').value);
    date_until = new Date(document.getElementById('date-until').value);

    map_plot_refresh();
}


function map_plot_refresh() {

    var filter = function(data) {
        var mission_begin = new Date(data[5].toString())
        var mission_end = new Date(data[6].toString())

        return heli_checked[data[9].toString()] &&
            stichwort_checked[data[2].toString()] &&
            date_from < mission_end && date_until > mission_begin;

    };

    draw_map(global_data, filter);

    draw_time_plot(global_data, 26, "time_t1", filter);
    draw_time_plot(global_data, 27, "time_t2", filter);
    draw_time_plot(global_data, 28, "time_patient", filter);
    draw_time_distribution(global_data, "time_distribution", filter);
    draw_time_line(global_data, "time_line", filter);
}


var global_data;

function draw_time_plot(data, time, time_string, filterFunction) {

    d3.selectAll('#'+time_string).remove();

    if(filterFunction != null) {
        data = data.filter(filterFunction);
    }
    var values = data.map(entry => parseInt(entry[time]));

    var formatCount = d3.format(",.0f");

    var margin = {top: 20, right: 50, bottom: 30, left: 30},
        width = 500 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

    var max = d3.max(values);
    var min = d3.min(values);
    var avg = values.reduce((a, b) => (a + b)) / values.length;

    var x = d3.scale.linear()
        .domain([min, max])
        .range([0, width]);

    var data = d3.layout.histogram()
        .bins(x.ticks(70))
    (values);

    var yMax = d3.max(data, function(d){return d.length});
    var yMin = d3.min(data, function(d){return d.length});

    var y = d3.scale.linear()
        .domain([0, yMax])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var svg = d3.select("#plot-"+time_string).append("svg")
        .attr("id", time_string)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .style("margin-left", "20px")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0)
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "underline")
        .text(time_string);

    bar = svg.selectAll(".bar")
        .data(data)
        .enter().append("g")
        .attr("class", "bar")
        .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });


    if(time_string == "time_t1") {
        bar.append("rect")
            .attr("x", 1)
            .attr("width", (x(data[0].dx) - x(0)) - 1)
            .attr("height", function(d) { return height - y(d.y); })
            .attr("fill", function(d) {
                r = Math.min(1, d.x/avg)*255;
                g = (1-d.x/max)*255;
                return d3.rgb(r,g,0);
            });
    }else {
        bar.append("rect")
            .attr("x", 1)
            .attr("width", (x(data[0].dx) - x(0)) - 1)
            .attr("height", function(d) { return height - y(d.y); })
            .attr("fill", function(d) {
                return d3.rgb(52, 152, 219)
            });
    }


    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);


}

function draw_time_distribution(data, desc, filterFunction) {

    d3.selectAll('#'+desc).remove();

    if(filterFunction != null) {
        data = data.filter(filterFunction);
    }

    var values = data.map(entry => new Date(entry[6].toString()).getHours());

    var formatCount = d3.format(",.0f");

    var margin = {top: 20, right: 50, bottom: 30, left: 40},
        width = 500 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;


    var x = d3.scale.linear()
        .domain([0, 24])
        .range([0, width]);

    var data = d3.layout.histogram()
        .bins(x.ticks(24))(values);

    var yMax = d3.max(data, function(d){return d.length});
    var yMin = d3.min(data, function(d){return d.length});

    var y = d3.scale.linear()
        .domain([0, yMax])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var svg = d3.select("#plot-"+desc).append("svg")
        .attr("id", desc)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", -5)
        .attr("text-anchor", "middle")
        .style("font-size", "14px")
        .style("text-decoration", "underline")
        .text("Verteilung 24h");

    bar = svg.selectAll(".bar")
        .data(data)
        .enter().append("g")
        .attr("class", "bar")
        .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

    bar.append("rect")
        .attr("x", 1)
        .attr("width", (x(data[0].dx) - x(0)) - 1)
        .attr("height", function(d) { return height - y(d.y); })
        .attr("fill", function(d) {
            return d3.rgb(52, 152, 219)
        });

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    var yAxis = d3.svg.axis().scale(y).orient("left");
    svg.append("g").attr("class", "y axis").call(yAxis);


}

function draw_time_line(data, desc, filterFunction) {

    d3.selectAll('#'+desc).remove();

    if(filterFunction != null) {
        data = data.filter(filterFunction);
    }

    // Count number of occurences per day and save them as tuples in result
    var dataset = {};
    for(var i = 0; i < data.length; i++) {
        var temp = new Date(data[i][6].toString());
        temp = temp.getFullYear() + "-" + (temp.getMonth() + 1) + "-" + temp.getDate();

        if(temp in dataset) {
            dataset[temp] += 1;
        }else{
            dataset[temp] = 1;
        }
    }

    var result = [];
    Object.keys(dataset).forEach(function(key) {
        var obj = {"date" : d3.time.format("%Y-%m-%d").parse(key), "count" : dataset[key]};
        result.push(obj);
    });

    // the date range of available data:
    var x_range = d3.extent(result, function(d) { return d.date; });
    // maximum date range allowed to display
    var mindate = x_range[0], maxdate = x_range[1];

    var svg = d3.select("#plot-"+desc).append("svg")
        .attr("id", desc)
        .attr("width", 1000)
        .attr("height", 300)
        .style("margin-left", "20px")
        .append("g")
        .attr("transform", "translate(" + 40 + "," + 20 + ")");

    var x = d3.time.scale()
        .range([0, 950])
        .domain([mindate, maxdate]);

    var x_axis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(d3.time.months, 1)
        .tickFormat(d3.time.format('%m/%Y'))//%Y-for year boundaries, such as 2011
        .tickSubdivide(1);//subdivide 12 months in a year

    svg.append("g").attr("transform", "translate(0,250)").call(x_axis);


    var y = d3.scale.linear()
        .domain([0, d3.max(result, function(d) { return d.count; })])
        .range([250, 0]);

    var y_axis = d3.svg.axis().scale(y).orient("left");



    svg.append("g").attr("class", "y axis").call(y_axis);

    svg.append("path")
        .datum(result)
        .attr("fill", "none")
        .attr("stroke", "steelblue")
        .attr("stroke-width", 1.5)
        .attr("d", d3.svg.line()
              .x(function(d) { return x(d.date) })
              .y(function(d) { return y(d.count) }));

    svg.append("text")
        .attr("x", 500)
        .attr("y", -5)
        .attr("text-anchor", "middle")
        .style("font-size", "14px")
        .style("text-decoration", "underline")
        .text("Einsätze/Tag");

}


function read_xlsx() {
    var fileUpload = document.getElementById("uploadfile");

    // validate whether File is valid Excel file.
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
    if(regex.test(fileUpload.value.toLowerCase())) {
        if(typeof (FileReader) != "undefined") {
            var reader = new FileReader();

            // for Browsers other than IE.
            if (reader.readAsBinaryString) {
                reader.onload = function (e) {
                    process_excel(e.target.result);
                };
                reader.readAsBinaryString(fileUpload.files[0]);
            } else {
                // for IE Browser
                reader.onload = function (e) {
                    var data = "";
                    var bytes = new Uint8Array(e.target.result);
                    for(var i = 0; i < bytes.byteLength; i++) {
                        data += String.fromCharCode(bytes[i]);
                    }
                    process_excel(data);
                };
                reader.readAsArrayBuffer(fileUpload.files[0]);
            }
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid Excel file.");
    }
}

function process_excel(data) {
    var workbook = XLSX.read(data, {type: 'binary'})
    var firstSheet = workbook.SheetNames[0]
    var excel = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet])

    var data = get_filtered_data(excel)

    global_data = data;
    draw_map(global_data);
    draw_time_plot(global_data, 26, "time_t1", null);
    draw_time_plot(global_data, 27, "time_t2", null);
    draw_time_plot(global_data, 28, "time_patient", null);
    draw_time_distribution(data, "time_distribution", null);
    draw_time_line(data, "time_line", null);
}

function get_filtered_data(excel) {
    relevant_keywords = ["KRAN", "SPPI", "VEST", "UNUN", "BEWE", "PRIM", "SPBI", "UNHA", "REAN", "BEUN", "GLAB", "SPOF", "ARAR", "UNTI", "BEKL", "SPSP", "ARBA", "UNVE", "UNER", "SPSC", "ARLA", "ARFO", "SPRE", "LAGA", "GLBA", "SPTA", "LAUN", "SPCA", "FLRE", "SPBA", "GLUN", "SPLA", "UNWA", "FLZI", "BESP", "HOHL", "VEBA", "LAVE", "UNIN", "EVSE", "SPMO", "SPSW", "FLMI", "EVMA", "GLKA", "EVEV"]

    excel = excel.filter(row => relevant_keywords.includes(row["Einsatzstichwort"]))
    excel = excel.filter(row => row["Ankunft_Einsatzort"] != "NULL")

    var t1s = excel.map(compute_t1)
    var t2s = excel.map(compute_t2)
    var tps = excel.map(compute_time_patient)

    var valid_times = (row, i) => (t1s[i] <= 75 && t1s[i] > 0 && t2s[i] < 50 && t2s[i] > 0 && tps[i] < 100)
    excel = excel.filter(valid_times)
    t1s = t1s.filter(valid_times)
    t2s = t2s.filter(valid_times)
    tps = tps.filter(valid_times)

    return excel.map((row, i) => [
        row["Einsatznummer_ELS"],
        row["Einsatznummer_SAP"],
        row["Einsatzstichwort"],
        row["Einsatzstichwort_Beschreibung"],
        row["Erstellungszeit"],
        row["Alarmeingang"],
        row["Abschlusszeit"],
        row["Monat"],
        row["Jahr"],
        row["Einsatzmittel"],
        row["Prim_Sek"],
        row["Helikopter_alarmiert"],
        row["Ankunft_Einsatzort"],
        row["Abflug_Einsatzort"],
        row["Ankunft_Zielort"],
        row["Einsatzort"],
        row["LV95_X"],
        row["LV95_Y"],
        row["WGS84_Latitude"],
        row["WGS84_Longitude"],
        row["Zielort"],
        row["SG_144"],
        row["Zielort_WGS84_Latitude"],
        row["Zielort_WGS84_Longitude"],
        row["Zielort_LV95_X"],
        row["Zielort_LV95_Y"],
        t1s[i],
        t2s[i],
        tps[i]
    ])

    return excel.map((row, i) => Object.values(row).concat([t1s[i], t2s[i], tps[i]]))
}

function compute_t1(row) {
    var time_start = row["Helikopter_alarmiert"] == "NULL" ? get_date(row["Alarmeingang"]) : get_date(row["Helikopter_alarmiert"])
    var time_arrival = row["Ankunft_Einsatzort"] < row["Abflug_Einsatzort"] ? get_date(row["Ankunft_Einsatzort"]) : get_date(row["Abflug_Einsatzort"])
    return (time_arrival - time_start) / 60000
}

function compute_t2(row) {
    var time_start = get_date(row["Abflug_Einsatzort"])
    var time_arrival = row["Ankunft_Zielort"] == "NULL" ? get_date(row["Abschlusszeit"]) : get_date(row["Ankunft_Zielort"])
    return (time_arrival - time_start) / 60000
}

function compute_time_patient(row) {
    var time_start = get_date(row["Ankunft_Einsatzort"])
    var time_end = get_date(row["Abflug_Einsatzort"])
    return (time_end - time_start) / 60000
}

function get_date(str) {
    return new moment(str, "MM/DD/YYYY HH:mm:ss").toDate()
}
