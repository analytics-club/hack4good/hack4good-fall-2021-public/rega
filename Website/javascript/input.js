var global_data;

function read_csv(input) {
    if(input.files && input.files[0]) {
        let reader = new FileReader();
        reader.readAsBinaryString(input.files[0]);
        reader.onload = function (e) {
            obj_csv.size = e.total;
            obj_csv.dataFile = e.target.result;
            csv_data = Papa.parse(obj_csv.dataFile).data;
            draw_map(csv_data);

            csv_data = csv_data.slice(1, -1);
            global_data = csv_data;
            draw_time_plot(csv_data, 27, "time_t1", null);
            draw_time_plot(csv_data, 28, "time_t2", null);
            draw_time_plot(csv_data, 29, "time_patient", null);
            draw_time_distribution(csv_data, "time_distribution", null);
            draw_time_line(csv_data, "time_line", null);
        }
    }
}
