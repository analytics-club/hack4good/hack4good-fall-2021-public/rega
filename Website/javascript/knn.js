import {distanceToBase, getDistanceKm, bases, mode, parseTimeToMinutes} from "./utils.js";
import {convexHull} from './convexhull.js';
import {graham_scan} from './grahamscan.js';


function get_column_by_name(header, name) {
    for (i = 0; i < header.length; i ++) {
        if (header[i] === name) {
            return i;
        }
    }
    console.log("WARNING: column not found", name);
    return undefined;
}

let einsatzmittel = ["Rega_1", "Rega_2", "Rega_3", "Rega_4", "Rega_5", "Rega_6", 
                     "Rega_7", "Rega_8", "Rega_9", "Rega_10", "Rega_12", "Rega_14", 
                     "Rega_15", "Rega_16", "Rega_17", "AGL_05", "AGL_06", "AGL_07", 
                     "AGL_08", "Gallus_3", "Robin_3", "HBE_ZMU", "HBE_ZMI", "HBE_ZMY", "SAR02", "CH_L"];

export function generate_service_regions(data) {
    console.log(data);
    var header = data[0];
    data = data.slice(1, -1);
    var col_heli = get_column_by_name(header, "Einsatzmittel");
    var col_time = get_column_by_name(header, "time_patient");
    var col_lat = get_column_by_name(header, "WGS84_Latitude");
    var col_lng = get_column_by_name(header, "WGS84_Longitude");
    console.log(data[0][col_lat]);

    data = data.filter(item => (!isNaN(item[col_lat])) && (!isNaN(item[col_lng])) );
    console.log(data[0][col_heli]);
    console.log(col_heli);

    var lines = [];
    var linesFromBase = new Map();
    var allPoints = [];

    for (var lat = 45.5; lat < 48.0; lat += 0.05) {
        for (var lng = 6.00; lng < 10.43; lng += 0.05) {
            var bestHeli = [];
            var worstHeli = [];
            var localData = data.filter(function(item) {
                return getDistanceKm(lat, lng, item[col_lat], item[col_lng]) < 18.0;
            })

            if (localData.length <= 12) {
                continue;
            }

            // Find closest points from data
            localData = localData.sort(function(a, b) {
                return a[col_time] - b[col_time];
                // var a_lat = a[col_lat]; var a_lng = a[col_lng];
                // var b_lat = b[col_lat]; var b_lng = b[col_lng];
                // return getDistanceKm(lat, lng, a_lat, a_lng) - getDistanceKm(lat, lng, b_lat, b_lng);
            });
            var dist = getDistanceKm(lat, lng, 
                localData[0][col_lat], localData[0][col_lng]);
            //var dist = distanceToBase("Rega_1", lat, lng);
            console.log("Min dist:", dist);

            var dist = getDistanceKm(lat, lng, 
                localData[localData.length - 1][col_lat], 
                localData[localData.length - 1][col_lng]);
            console.log("Max dist:", dist);

            for (var i = 0; i < 12; i++) {
                bestHeli.push(localData[i][col_heli]);
                worstHeli.push(localData[localData.length - 1][col_heli]);
            }
            console.log("Best heli:", bestHeli);
            console.log("Worst heli:", worstHeli);

            try {
                lines.push([lat, lng, bases[bestHeli[0]]['lat'], bases[bestHeli[0]]['lon']]);
                allPoints.push([lat, lng, bestHeli]);
            } catch {}

            try {
                var baseKey = mode(bestHeli);
                if (!linesFromBase.has(baseKey)) {
                    linesFromBase.set(baseKey, bases[baseKey]);
                    linesFromBase.get(baseKey)['points'] = [];
                }
                linesFromBase.get(baseKey)['points'].push([lat, lng]);
            } catch {}
        }
    }

    linesFromBase.forEach(function(value, key, map) {
        try {
            map.get(key)['hull'] = graham_scan(value['points']);
        } catch {}
    });
    return allPoints;//linesFromBase;
}


export function save_service_regions(data) {
    var filename = "service_regions.json";
    var text = generate_service_regions(data);
    var element = document.createElement('a');
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(text));
    element.setAttribute('href', dataStr);
    element.setAttribute('download', filename);
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();
  
    document.body.removeChild(element);
}


export function generate_heatmap(data, which) {
    var header = data[0];
    data = data.slice(1, -1);
    var col_heli = get_column_by_name(header, "Einsatzmittel");
    var col_time = -1;
    if (which == "t1a") {
        col_time = get_column_by_name(header, "time_patient");
    } else if (which == "t1c") {
        col_time = get_column_by_name(header, "time_on_site");
    } else if (which == "t1b") {
        col_time = get_column_by_name(header, "time_until_landing");
    }
    var col_lat = get_column_by_name(header, "WGS84_Latitude");
    var col_lng = get_column_by_name(header, "WGS84_Longitude");
    console.log(data[0][col_lat]);

    data = data.filter(item => (!isNaN(item[col_lat])) && (!isNaN(item[col_lng])) );
    console.log(data[0][col_heli]);
    console.log(col_heli);

    var avgTimePerPoint = [];

    for (var lat = 45.5; lat < 48.0; lat += 0.05) {
        for (var lng = 6.00; lng < 10.43; lng += 0.05) {

            var localData = data.filter(function(item) {
                return getDistanceKm(lat, lng, item[col_lat], item[col_lng]) < 12.0;
            })

            if (localData.length <= 6) {
                continue;
            }

            // Find closest points from data
            // localData = localData.sort(function(a, b) {
                // return a[col_time] - b[col_time];
            // });
            // try {
                var timeSum = 0;
                var validCnt = 0;
                console.log(localData.length);
                for (i = 0; i < localData.length; i++) {
                    var minutes = parseTimeToMinutes(localData[i][col_time]);
                    if (!isNaN(minutes)) {
                        timeSum += minutes;
                        validCnt += 1;
                    }
                }
                if (validCnt > 0) {
                    timeSum /= validCnt;
                    avgTimePerPoint.push([lat, lng, timeSum]);
                    console.log(timeSum);
                }
            // } catch {
                // continue;
            // }
        }
    }
    return avgTimePerPoint;
}

export function save_heatmap(data, which) {
    var filename = "heatmap." + which + ".json";
    var text = generate_heatmap(data, which);
    var element = document.createElement('a');
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(text));
    element.setAttribute('href', dataStr);
    element.setAttribute('download', filename);
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();
  
    document.body.removeChild(element);
}
