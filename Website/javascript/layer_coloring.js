
function htmlToElem(html) {
    let temp = document.createElement('template');
    html = html.trim();
    temp.innerHTML = html;
    return temp.content.firstChild;
}

// add einsatzmittel checkboxes to DOM
let einsatzmittel = ["Rega_1", "Rega_2", "Rega_3", "Rega_4", "Rega_5", "Rega_6", "Rega_7", "Rega_8", "Rega_9", "Rega_10", "Rega_12", "Rega_14", "Rega_15", "Rega_16", "Rega_17", "AGL_05", "AGL_06", "AGL_07", "AGL_08", "Gallus_3", "Robin_3", "HBE_ZMU", "HBE_ZMI", "HBE_ZMY", "SAR02", "CH_L"];
for(var i = 0; i<einsatzmittel.length; i++) {
    let emCheckbox = htmlToElem('<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="'+einsatzmittel[i]+'">'+
                                '<input type="checkbox" id="'+einsatzmittel[i]+'" class="mdl-checkbox__input" onchange="helicopterSelected(this)" checked>'+
                                '<span class="mdl-checkbox__label">'+einsatzmittel[i]+'</span>'+
                                '</label>');
    //  document.getElementById('einsatzmittelSelection').appendChild(emCheckbox);
}

// add einsatzstichwort checkboxes to DOM
let stichworte = ["KRAN", "SPPI", "VEST", "UNUN", "BEWE", "PRIM", "SPBI", "UNHA", "REAN", "BEUN", "GLAB", "SPOF", "ARAR", "UNTI", "BEKL", "SPSP", "ARBA", "UNVE", "UNER", "SPSC", "ARLA", "ARFO", "SPRE", "LAGA", "GLBA", "SPTA", "LAUN", "SPCA", "FLRE", "SPBA", "GLUN", "SPLA", "UNWA", "FLZI", "BESP", "HOHL", "VEBA", "LAVE", "UNIN", "EVSE", "SPMO", "SPSW", "FLMI", "EVMA", "GLKA", "EVEV"];
for(var i = 0; i<stichworte.length; i++) {
    let swCheckbox = htmlToElem('<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="'+stichworte[i]+'">'+
                                '<input type="checkbox" id="'+stichworte[i]+'" class="mdl-checkbox__input" onchange="stichwortSelected(this)" checked>'+
                                '<span class="mdl-checkbox__label">'+stichworte[i]+'</span>'+
                                '</label>');
    //  document.getElementById('stichwortSelection').appendChild(swCheckbox);
}


var map = L.map('map').setView([46.79, 8.32], 8);
var heli_layers = {};

// add helicopter bases to map
var bases = [
    {name: "Rega-Center", lat: 47.457990, lon: 8.572991},
    {name: "Rega 1", lat: 47.395876, lon: 8.637898},
    {name: "Rega 2", lat: 47.605896, lon: 7.523330},
    {name: "Rega 3", lat: 46.909619, lon: 7.504851},
    {name: "Rega 4", lat: 46.547337, lon: 6.618223},
    {name: "Rega 5", lat: 46.912776, lon: 9.551239},
    {name: "Rega 6", lat: 46.163171, lon: 8.881034},
    {name: "Rega 7", lat: 47.405521, lon: 9.290079},
    {name: "Rega 8", lat: 46.834069, lon: 8.638286},
    {name: "Rega 9", lat: 46.530427, lon: 9.878443},
    {name: "Rega 10", lat: 46.670054, lon: 7.876414},
    {name: "Rega 12", lat: 47.078150, lon: 9.066240},
    {name: "Rega 14", lat: 46.554903, lon: 7.379185},
    {name: "Rega 15", lat: 46.233339, lon: 6.096148},
    {name: "Trainingsbasis Grenchen", lat: 47.181693, lon: 7.411192}
];

var heli_icon = L.icon({
    iconUrl: 'images/heli_icon.png',
    iconSize:     [24, 24], // size of the icon
});
for(var i = 0; i<bases.length; i++) {
    L.marker([bases[i].lat, bases[i].lon], {icon: heli_icon}).bindPopup(bases[i].name).addTo(map);
}

L.tileLayer('https://tile.osm.ch/switzerland/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18
}).addTo(map);

var mission_markers = [];
var global_data;

// rest is for reading csv file...
var obj_csv = {
    size: 0,
    dataFile: []
};

function drawRectAround(lat, lng, color, opacity) {
    var x1 = lat - 0.025;
    var x2 = lat + 0.025;
    var y1 = lng - 0.025;
    var y2 = lng + 0.025;
    L.polygon([[x1,y1], [x1,y2], [x2, y2], [x2, y1]], {
        'color': color, 'opacity': 0.0, 'fillOpacity': opacity
    }).addTo(map);
}

var colors =  {
    "Rega_1": "#00bfb2",
    "Rega_2": "#80B918",
    "Rega_3": "#e4ff1a",
    "Rega_4": "#00bfb2",
    "Rega_5": "#ff5714",
    "Rega_6": "#5f00ba",
    "Rega_7": "#CF1259",
    "Rega_8": "#1789fc",
    "Rega_9": "#484349",
    "Rega_10" : "#f7f0f0",

    "Rega_11" : "#00bfb2",
    "Rega_12" : "#80B918",
    "Rega_13" : "#e4ff1a",
    "Rega_14" : "#ffb800",
    "Rega_15" : "#ff5714",
    "Rega_16" : "#5f00ba",
    "Rega_17" : "#CF1259",
    "Rega_18" : "#1789fc",
    "Rega_19" : "#484349",
    "Rega_20" : "#f7f0f0"
}

function mode(arr){
    return arr.sort((a,b) =>
    arr.filter(v => v===a).length
    - arr.filter(v => v===b).length
    ).pop();
}

function mode2(arr){
    return arr.sort((a,b) =>
    arr.filter(v => v===a).length
    - arr.filter(v => v===b).length
    ).pop();
}

function drawServiceRegionLayer(lines) {
    lines.forEach(function(point) {
                        var base = mode(point[2]);
                        var color = "white";
                        if ("undefined" === typeof(colors[base])) {
                            console.log("Undefined", base);
                        } else {
                            color = colors[base];
                        }
                        drawRectAround(point[0], point[1], color, 0.4);
                    });
}

function drawHeatmapLayer(heatmapData, which) {
    import('../libs/rainbowvis.js').then(function(obj) {
        // Add heatmap
        rainbow = new obj.Rainbow();
        if (which == 't1a') {
            rainbow.setNumberRange(0, 23.5);
        } else if (which == 't1c') {
            rainbow.setNumberRange(20, 40);
        } else if (which == 't1b') {
            rainbow.setNumberRange(0, 10);
        }
        rainbow.setSpectrum('green', 'yellow', 'red');
        heatmapData.forEach(function(point) {
            var avgTime = point[2];
            console.log(avgTime);
            var color = "#" + rainbow.colourAt(avgTime);
            drawRectAround(point[0], point[1], color, 0.8);
        });

        // Add legend
        var legend = L.control({position: 'topright'});
        legend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'Average mission time');
            // div.innerHTML = "Average mission time"
            var minI = 0;
            var maxI = 0;
            if (which == 't1a') {
                minI = 0; maxI = 30;
            } else if (which == 't1c') {
                minI = 20; maxI = 45;
            }

            for (var i = minI; i < maxI; i+=5) {
                div.innerHTML += '<div style="background: ' + '#' + rainbow.colourAt(i) +
                                    '; width: 15px; height: 15px;' + '"/>'
            //     div.innerHTML += 
            //     labels.push(
            //         '<i class="square" style="background:' + "red;" + '"></i> ' +
            //     (categories[i] ? categories[i] : '+'));
            }
            // div.innerHTML = div;//labels.join('FOO<br>');
            return div;
        };
        legend.addTo(map);
    });
}

function load_service_regions() {
    $.ajax({
        url: "/data/service_regions.json",
        async: false,
        dataType: 'json',
        success: function(data) {
            drawServiceRegionLayer(data);
        }
    });
}

function load_heatmap(which) {
    $.ajax({
        url: "/data/heatmap." + which + ".json",
        async: false,
        dataType: 'json',
        success: function(data) {
            drawHeatmapLayer(data, which);
        }
    });
}

function read_csv(input) {

    if(input.files && input.files[0]) {
        let reader = new FileReader();
        reader.readAsBinaryString(input.files[0]);
        reader.onload = function (e) {
            obj_csv.size = e.total;
            obj_csv.dataFile = e.target.result;
            csv_data = Papa.parse(obj_csv.dataFile).data;
            
            import ("./knn.js")
                .then(function(obj) {
                    // obj.save_heatmap(csv_data, 't1a');
                    // obj.save_heatmap(csv_data, 't1b');
                    // obj.save_heatmap(csv_data, 't1c');
                    // var heatmap = obj.generate_heatmap(csv_data, 't1a');
                    // drawHeatmapLayer(heatmap);
                    // obj.save_service_regions(csv_data);
                    // obj.load_service_regions(drawServiceRegionLayer);
                    // var lines = obj.generate_service_regions(csv_data);
                });
        

        }
    }
}

// load_service_regions();
load_heatmap('t1a');
// load_heatmap('t1c');
