# Rega: Real-time rescue helicopter dispatch decision support

In Switzerland, people hike a lot and in a normal year, 20,000 people are injured, and almost 40 incidents per year are fatal. That's why Rega, the Swiss air rescue team, is constantly striving to make its rescue service faster and more reliable. Based on a patient's location, our Lasso model, trained on previous missions, predicts how long it would take each helicopter to reach the destination and then sends the fastest one.

## Repository
* API: This simple RESTful API allows prediction requests to be made over HTTP(S).
* Website: This website calls the API and visualizes the predictions on a map.
* final_code: These notebooks allow the user to reproduce the results: feature creation model training and evaluation.

